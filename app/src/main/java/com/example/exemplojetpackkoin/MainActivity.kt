package com.example.exemplojetpackkoin

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.exemplojetpackkoin.mvp.Contract
import com.example.exemplojetpackkoin.mvp.Presenter
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf


class MainActivity : AppCompatActivity(), Contract.View {


    val mPresenter: Contract.Presenter by inject<Presenter> { parametersOf(this)  }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }



    fun buttonInsert(v: View){
        mPresenter.insertData("Nome Cadastrado")
    }

    fun buttonDelete(v: View){
        mPresenter.deleteData("Nome Deletado")
    }


    override fun sucess() {
        Log.i("RESPONSE","sucesso")
    }

}
