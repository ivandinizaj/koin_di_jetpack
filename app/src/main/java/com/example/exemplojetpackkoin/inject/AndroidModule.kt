package com.example.exemplojetpackkoin.inject

import android.content.Context
import com.example.exemplojetpackkoin.MainActivity
import com.example.exemplojetpackkoin.Repository
import com.example.exemplojetpackkoin.mvp.Contract
import com.example.exemplojetpackkoin.mvp.Presenter
import org.koin.dsl.module

//Modulo do PRESENTER
val androidModule = module {
    single { this }

    // instancia unica (um singleton)
    single {
        Repository(context = get())
    }

    // vai instanciar varias vezes o PRESENTER
    factory {
         Presenter(mView = get (), repository = get())
    }
}