package com.example.exemplojetpackkoin.inject

import com.example.exemplojetpackkoin.MainActivity
import com.example.exemplojetpackkoin.mvp.Contract
import org.koin.dsl.module


//Modulo da VIEW
val androidModuleUI = module {

    // vai instanciar varias vezes o VIEW
    factory { MainActivity() as Contract.View }
}