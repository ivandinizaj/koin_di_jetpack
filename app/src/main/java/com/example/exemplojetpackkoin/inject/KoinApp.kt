package com.example.exemplojetpackkoin.inject

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.module.Module

class KoinApp: Application() {

    override fun onCreate() {
        super.onCreate()


        startKoin {
            // Android context
            androidContext(this@KoinApp)
            // modules
            modules(listOf(androidModule,androidModuleUI))
        }
    }

    override fun onTerminate() {
        super.onTerminate()
        stopKoin()
    }
}