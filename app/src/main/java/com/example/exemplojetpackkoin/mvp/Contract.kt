package com.example.exemplojetpackkoin.mvp

interface Contract {

    interface Presenter{
        fun insertData(value:String)
        fun deleteData(value:String)
    }


    interface View{
        fun sucess()
    }

}