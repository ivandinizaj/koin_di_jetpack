package com.example.exemplojetpackkoin.mvp

import android.content.Context
import com.example.exemplojetpackkoin.Repository

class Presenter(var mView: Contract.View, var repository: Repository): Contract.Presenter {


    override fun insertData(value: String) {
        repository.insert(value)
    }

    override fun deleteData(value: String) {
        repository.delete(value)
        mView.sucess()
    }
}